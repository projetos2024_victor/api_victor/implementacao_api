const axios = require("axios");

module.exports = class JSONPlaceholderController {
  static async getUsers(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data;
      resa
        .status(200)
        .json({
          message:
            "Aqui estão os usuários captados da Api pública JSONPlaceholder ",
          users,
        });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Falha ao encontrar usuários" });
    }
  }

  static async getUsersWebSiteIO(req, res) {
    try{
      const response = await axios.get("https://jsonplaceholder.typicode.com/users");
      const users = response.data.filter( 
        (user) => user.website.endsWith(".io")
      )
      const banana = users.length
        res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários com domínio .io",
          users, banana
        });
    }
    catch(error){
      res.status(500).json({ error: "Deu ruim" });
    }
  }

  static async getUsersWebSiteCOM(req, res) {
    try{
      const response = await axios.get("https://jsonplaceholder.typicode.com/users");
      const users = response.data.filter( 
        (user) => user.website.endsWith(".com")
      )
      const usuarios = users.length
        res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários com domínio .com",
          users, usuarios
        });
    }
    catch(error){
      res.status(500).json({ error: "Deu ruim" });
    }
  }

  static async getUsersWebSiteNET(req, res) {
    const response = await axios.get("https://jsonplaceholder.typicode.com/users");
      const users = response.data.filter( 
        (user) => user.website.endsWith(".net")
      )
      const usuarios = users.length
        res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários com domínio .net",
          users, usuarios
        });
    }
    catch(error){
      res.status(500).json({ error: "Deu ruim" });
    }

    static async getCountDomain(req, res) {
      const { dominio } = req.query;
    
      try {
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/users"
        );
        const users = response.data.filter((user) => user.website.endsWith(`.${dominio}`));
        const Banana = users.length;
    
        res.status(200).json({
          message: `AQUI ESTÃO OS USERS COM DOMINIO ${dominio.toUpperCase()}. Quantos bananas tem esse dominio: ${Banana}`,
          users,
        });
      } catch (error) {
        console.log(error);
        res.status(500).json({ error: "Deu ruim", error });
      }
    }
  
};
