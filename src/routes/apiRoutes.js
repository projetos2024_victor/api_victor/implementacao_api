const router = require('express').Router()
const teacherController = require("../controller/teacherController")
const JSONPlaceholderController = require("../controller/JSONPlaceholderController")


// router.get('/teacher/', teacherController.getAluno);
router.post('/cadastroAluno/:_id', teacherController.postAluno);
// router.put('/updateAluno/', teacherController.updateAluno);
// router.delete('/deleteAluno/', teacherController.deleteAluno);

router.get("/external/", JSONPlaceholderController.getUsers);
// router.get('/external/io', JSONPlaceholderController.getUsersWebsiteIO)
// router.get('/external/com', JSONPlaceholderController.getUsersWebsiteCOM)
// router.get('/external/net', JSONPlaceholderController.getUsersWebsiteNET)
router.get('/external/filter/', JSONPlaceholderController.getCountDomain)



module.exports = router;